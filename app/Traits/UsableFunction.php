<?php
 
namespace App\Traits;
use Illuminate\Support\Facades\Validator;
 
trait UsableFunction
{
    protected function Validator(array $data, array $role, $first = true)
    {
        $validator = Validator::make($data, $role);
        if ($validator->fails()) {
            if (!$first) {
                $errors = $validator->getMessageBag()->first();

                foreach ($errors as $i=>$error) {
                    $errormsg[]['error'] = $error;
                }
                return $errormsg;
            }
            $errors = $validator->getMessageBag()->first();
            return $errors;
        }
    }
    protected function ResponseStatus($status_code, $message, $data)
    {
        return response()->json([
            'STATUS' => $status_code,
            'MESSAGE' => $message,
            'DATA' => $data
        ], $status_code);
    }
}