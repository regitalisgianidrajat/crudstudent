<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StudentModel;
use App\Traits\UsableFunction;
use App\Transformers\StudentTransformer;

class StudentController extends Controller
{
    private $student;
    use UsableFunction;

    public function __construct()
    {
        $this->student  = StudentModel::select('*')->orderBy('student_id', 'desc');
    }
    public function index()
    {
        $data  = $this->student->get();
        if (!$data->isEmpty()) {
            return $this->ResponseStatus(200, 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus(404, 'FAILED DATA NOT FOUND', array());
        }
        
    }
    public function detail($id)
    {
        $data  = $this->student->where('student_id',$id)->with('city')->first();

        if ($data) {
            $data->makeVisible(['status']);
            $data['status_desc']    = ($data['status']==0) ? 'Active' : 'Deadactive';
            return $this->ResponseStatus(200, 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus(404, 'FAILED DATA NOT FOUND', new \stdClass());
        }
        
    }
    public function store(Request $request)
    {
        $PostRequest = $request->only('name','email','phone','city','address','date_birth');
        $role = [
            'email'         => 'unique:student|email',            
            'name'          => 'Required',
            'phone'         => 'Required|numeric|digits_between:1,13',
            'city'          => 'Required|numeric',
            'address'       => 'Required',
            'date_birth'    => 'Required|date'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus(400, $ErrorMsg, new \stdClass());
        }
        
        $saved = StudentModel::create($PostRequest);

        if(!$saved){
            return $this->ResponseStatus(400, 'FAILED DATA NOT SAVED', new \stdClass());
        }
        return $this->ResponseStatus(200, 'SUCCESS', new \stdClass());
    }
    public function update(Request $request)
    {
        $PostRequest = $request->only('name','phone','city','address','date_birth');
        $role = [
            'student_id'    => 'Required',
            'name'          => 'Required',
            'phone'         => 'Required|numeric|digits_between:1,13',
            'city'          => 'Required',
            'address'       => 'Required',
            'date_birth'    => 'Required|date'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus(400, 'FAILED '.$ErrorMsg, new \stdClass());
        }
        $saved = StudentModel::where('student_id',$request['student_id'])->update($PostRequest);

        if(!$saved){
            return $this->ResponseStatus(400, 'FAILED DATA NOT SAVED', new \stdClass());
        }
        return $this->ResponseStatus(200, 'SUCCESS', new \stdClass());
        
    }
    public function destroy($id)
    {
        $saved = $this->student->where('student_id',$id)->delete();
        if(!$saved){
            return $this->ResponseStatus(400, 'FAILED DATA NOT DELETED', new \stdClass());
        }
        return $this->ResponseStatus(200, 'SUCCESS', new \stdClass());
        
    }
    public function status(Request $request)
    {
        $PostRequest = $request->only('status');
        $role = [
            'student_id'        => 'Required',
            'status'            => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus(400, 'FAILED '.$ErrorMsg, new \stdClass());
        }
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }
        $saved = StudentModel::where('student_id',$request['student_id'])->update($PostRequest);

        if(!$saved){
            return $this->ResponseStatus(400, 'FAILED DATA NOT SAVED', new \stdClass());
        }
        return $this->ResponseStatus(200, 'SUCCESS', new \stdClass());
        
    }
    public function search(Request $request)
    {
        
        $data  = $this->student->where('name', 'like', '%' . $request['name'] . '%')->with('city')->get();
        if(!empty($request['status'])){
            $data  = $this->student->where('name', 'like', '%' . $request['name'] . '%')->where('status',$request['status'])->with('city')->get();
        }

        if (!$data->isEmpty()) {
            return $this->ResponseStatus(200, 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus(404, 'FAILED DATA NOT FOUND', new \stdClass());
        }
        
    }

}
