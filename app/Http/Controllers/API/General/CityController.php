<?php

namespace App\Http\Controllers\API\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\General\CityModel;
use App\Traits\UsableFunction;

class CityController extends Controller
{
    private $city;
    use UsableFunction;

    public function __construct()
    {
        $this->city     = CityModel::select('*');
    }
    public function index()
    {
        $data  = $this->city->get();
        if (!$data->isEmpty()) {
            return $this->ResponseStatus(200, 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus(404, 'FAILED DATA NOT FOUND', array());
        }
        
    }


}
