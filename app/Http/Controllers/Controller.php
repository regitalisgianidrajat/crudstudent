<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Firebase\JWT\JWT;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function jwt($credential) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $credential->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60*24,// Expiration time
            'name' => $credential->name,
            'secret' => $credential->secret,
            'user' => $credential->user,

        ];
        
        $response['TOKEN'] = JWT::encode($payload, env('JWT_SECRET'));
        // $response['token_credential'] = JWT::encode($payload, env('JWT_SECRET'), array('HS256'));
        return $this->ResponseStatus(200, 'Success! Token Generated !', $response);
    }

}
