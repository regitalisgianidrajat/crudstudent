<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if (!$request->wantsJson()) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => 'Unauthorized access, Please input token',
                'STATUS' => 400
            ], 400);
        }
        if ($e instanceof ValidationException) {
            foreach ($e->errors() as $key => $error) {
                $data_error[] = $error[0];
            }
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => $data_error[0]. ' (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 501
            ], 200);
        } else if ($e instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => 'Invalid Token ('.$e->getMessage().')',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 2001
            ], 200);
        } else if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => 'Page Not Found (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 404
            ], 200);
        } else if($e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => 'Method Not Allowed (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 400
            ], 200);
        } else if ($e instanceof \Illuminate\Database\QueryException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => $e->getMessage(). ' (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 500
            ], 200);
        } else if ($e instanceof ModelNotFoundException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => 'Data Not Found (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 404
            ], 200);
        } else if ($e instanceof \BadMethodCallException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => $e->getMessage(). ' (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 400
            ], 200);
        } else if($e instanceof \ErrorException) {
            return response()->json([
                'DATA' => (Object) [],
                'MESSAGE' => $e->getMessage(). ' (SVCTRANS)',
                'STATUS' => method_exists($e, 'getStatusCode') ? $e->getStatusCode() : 500
            ], 500);
        }
        return parent::render($request, $exception);
    }
}
