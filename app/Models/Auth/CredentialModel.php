<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class CredentialModel extends Model
{
    protected $table   = 'credential';
	public $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
		'name',
		'user',
		'secret'
	];
	protected $casts = [
		'name' 	=> 'string',
		'user' 	=> 'string',
		'secret'=> 'string'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at'
    ];
}
