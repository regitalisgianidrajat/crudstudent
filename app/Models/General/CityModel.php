<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table   = 'city';
	public $primarykey = 'city_id';
    public $timestamps = true;
    protected $fillable = [
		'city_name'
	];
	protected $casts = [
		'city_name' 	=> 'string'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at'
    ];
    public function task()
    {
        return $this->hasOne('App\Models\StudentModel','city_id', 'city_id');
    }

}
