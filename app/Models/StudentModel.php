<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentModel extends Model
{
    use SoftDeletes;
    protected $table   = 'student';
    public $primarykey = 'student_id';
    
    public $timestamps = true;

    protected $fillable = [
		'name',
		'email',
        'phone',
		'date_birth',
		'status',
		'city',
		'address',
	];
	protected $casts = [
		'name' 	    => 'string',
		'email' 	=> 'string',
		'phone'     => 'integer',
		'city'      => 'integer',
		'address'   => 'string',
		'status'    => 'integer',
    ];
		
	protected $hidden = [
		'address',
		'status',
		'created_at',
		'deleted_at',
		'updated_at'
    ];
    public function city()
    {
        return $this->hasOne('App\Models\General\CityModel', 'city_id', 'city');
    }

}
