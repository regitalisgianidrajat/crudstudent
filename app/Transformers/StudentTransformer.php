<?php
namespace App\Transformers;

use App\Models\StudentModel;
use League\Fractal;

class StudentTransformer extends Fractal\TransformerAbstract
{
    public function transform($student)
    {
        return [
            'student_id'            => (int) $student->student_id,
            'name'                  => (string) $student->name,
            'email'                 => (string) $student->email,
            'city'                  => (string) $student [city]['city_name']
        ];
    }
}