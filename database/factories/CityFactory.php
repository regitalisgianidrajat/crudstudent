<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\General\CityModel;
use Faker\Generator as Faker;

$factory->define(CityModel::class, function (Faker $faker) {
    return [
        'city_name' => $faker->city
    ];
});
