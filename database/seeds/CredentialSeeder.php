<?php

use Illuminate\Database\Seeder;

class CredentialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \App\Models\Auth\CredentialModel::create([
            'name'    => 'Klik Digital Sinergi',
            'user'    => 'Klik Digital',
            'secret'  => 'KlikDigital'
        ],);
    }
}
