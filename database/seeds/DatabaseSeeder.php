<?php

use Illuminate\Database\Seeder;
use App\Models\General\CityModel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(CityModel::class, 5)->create();
        $this->call(CredentialSeeder::class);
    }
}
