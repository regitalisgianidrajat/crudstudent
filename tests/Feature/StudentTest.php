<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testShouldReturnAllStudent(){

        $response = $this->get("api/student", []);
        $response
            ->assertStatus(200)
            ->assertJson([
                'STATUS' => true,
                'MESSAGE' => true,
                'DATA' => true,
            ]);
        $response_detail = $this->get("api/student/1", []);
        $response_detail
            ->assertStatus(200)
            ->assertJson([
                'STATUS' => true,
                'MESSAGE' => true,
                'DATA' => true,
            ]);
        $response_city = $this->get("api/city", []);
        $response_city
            ->assertStatus(200)
            ->assertJson([
                'STATUS' => true,
                'MESSAGE' => true,
                'DATA' => true,
            ]);
            
        
    }
}
