<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//credential
Route::post('api/get-token','API\Auth\CredentialController@create');
// API
Route::group(['middleware' => ['checkToken'],'prefix' => 'api'], function () {
    //student
    Route::get('student','API\StudentController@index');
    Route::get('student/{id}','API\StudentController@detail');
    Route::post('student/create','API\StudentController@store');
    Route::put('student/update','API\StudentController@update');
    Route::delete('student/{id}','API\StudentController@destroy');
    Route::post('student/search','API\StudentController@search');
    Route::post('student/status','API\StudentController@status');
    //general
    Route::get('city','API\General\CityController@index');
});
